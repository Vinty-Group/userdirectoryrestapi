// Создаем input и добавляем его на страницу
let input = document.createElement('input');
input.type = 'text';
input.placeholder = 'Поиск по ФИО, Должности, Телефону...';
document.body.appendChild(input);
input.focus();
search();

// Функция для отправки запроса при изменении значения в input-е
function search() {
    let value = input.value;
    let url = `http://localhost:8080/exell/officeBook?find=${value}`;
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = function() {
        if (xhr.status === 200) {
            let data = JSON.parse(xhr.responseText);
            let tableBody = document.getElementById('table-body');
            tableBody.innerHTML = ''; // Очищаем таблицу перед заполнением новыми данными
            for (var key in data) {
                let row = document.createElement('tr');
                let jobTitleCell = document.createElement('td');
                let responsibleForCell = document.createElement('td');
                let fullNameCell = document.createElement('td');
                let phoneCell = document.createElement('td');
                jobTitleCell.innerText = data[key].officeCells.jobTitle;
                responsibleForCell.innerText = data[key].officeCells.responsibleFor;
                fullNameCell.innerText = data[key].officeCells.fullName;
                phoneCell.innerText = data[key].officeCells.phone;

                row.appendChild(jobTitleCell);
                row.appendChild(responsibleForCell);
                row.appendChild(fullNameCell);
                row.appendChild(phoneCell);
                tableBody.appendChild(row);
                if (data[key].typeRecord === "$$header") {
                    row.classList.add('header');
                }
                if (data[key].typeRecord === "$$subHeader") {
                    row.classList.add('subHeader');
                }
            }
        } else {
            console.log('Ошибка загрузки данных');
        }
    };
    xhr.send();
}

// Отслеживаем изменения значения в input-е каждые 500мс
let timeoutId;
input.addEventListener('input', function() {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(search, 250);
});