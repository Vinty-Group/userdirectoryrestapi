--drop table user_directory.user_entity ;
--drop table user_directory.phones ;
--drop table user_directory.city  ;
--drop table user_directory.network_operator ;


CREATE TABLE user_directory.city (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	city_name varchar(100) NULL,
	CONSTRAINT city_pkey PRIMARY KEY (id)
);

CREATE TABLE user_directory.network_operator (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	network_operator_name varchar(100) NULL,
	CONSTRAINT network_operator_pkey PRIMARY KEY (id)
);

CREATE TABLE user_directory.phones (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	phone_number varchar(20) NULL,
	user_entity_id int8 NOT NULL,
	CONSTRAINT phones_pkey PRIMARY KEY (id)
);

ALTER TABLE user_directory.phones ADD CONSTRAINT phones_fk FOREIGN KEY (user_entity_id) REFERENCES user_directory.user_entity(id);

CREATE TABLE user_directory.user_entity (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	code varchar NULL,
	city_id int8 NULL,
	address varchar NULL,
	"location" varchar NULL,
	network_operator_id int8 NULL,
	work_schedule varchar NULL,
	phone_id int8 NULL,
	chief varchar NULL,
	CONSTRAINT user_entity_pkey PRIMARY KEY (id)
);

ALTER TABLE user_directory.user_entity ADD CONSTRAINT user_entity_city_id_fk FOREIGN KEY (city_id) REFERENCES user_directory.city(id);
ALTER TABLE user_directory.user_entity ADD CONSTRAINT user_entity_network_operator_id_fk FOREIGN KEY (network_operator_id) REFERENCES user_directory.network_operator(id);



INSERT INTO user_directory.city (city_name) VALUES
	 ('Минск'),
	 ('Могилев');
INSERT INTO user_directory.network_operator (network_operator_name) VALUES
	 ('Без оператора'),
	 ('Life'),
	 ('A1'),
	 ('MTC');
INSERT INTO user_directory.phones (phone_number,user_entity_id) VALUES
	 ('375 29 303 03 03',1),
	 ('375 44 588 26 08',8),
	 ('375 44 590 23 16',19),
	 ('+375296244744',19);
INSERT INTO user_directory.user_entity (code,city_id,address,"location",network_operator_id,work_schedule,phone_id,chief) VALUES
	 ('Интернет-магазин',1,'пер. Козлова, 25',NULL,1,'09.00 - 21.00',NULL,NULL),
	 ('С 43',2,' ул. Мовчанского, 6','ТЦ Гиппо',2,'10.00 - 22.00',3,'Воробей Александр'),
	 ('C 44',2,' ул. Лазаренко, 73Б','Евроопт',2,'10.00 - 21.00',10,'Воробей Александр');


----------------------------------------------------------
--   Procedures
----------------------------------------------------------
-- Для Хибернейта
   CREATE OR REPLACE
FUNCTION user_directory.get_users_h()
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
DECLARE
    ref_cursor REFCURSOR := 'mycursor';

BEGIN
    OPEN ref_cursor FOR
SELECT
	ue.id,
	ue.code,
	c.city_name,
	ue.address,
	ue."location",
	n.network_operator_name,
	ue.work_schedule,
	p.phone_number,
	ue.chief
FROM
	user_directory.user_entity ue
LEFT JOIN user_directory.city c ON
	c.id = ue.city_id
LEFT JOIN user_directory.phones p ON
	p.user_entity_id = ue.id
LEFT JOIN user_directory.network_operator n ON
	n.id = p.user_entity_id;

RETURN (ref_cursor);
END;

$function$
;
----------------------------------------------------------
-- Для SimpleJdbcCall
----------------------------------------------------------

CREATE OR REPLACE
FUNCTION user_directory.get_users(
    OUT p_resp_code varchar ,
    OUT ref_cursor REFCURSOR
) 
AS $$
DECLARE

BEGIN
	p_resp_code := 'OK';

OPEN ref_cursor FOR
SELECT
	DISTINCT ue.id,
	ue.code,
	c.city_name,
	ue.address,
	ue."location",
	n.network_operator_name,
	ue.work_schedule,
	ue.PHONE_ID ,
	ue.chief
FROM
	user_directory.user_entity ue
LEFT JOIN user_directory.city c ON
	c.id = ue.city_id
LEFT JOIN user_directory.phones p ON
	p.user_entity_id = ue.id
LEFT JOIN user_directory.network_operator n ON
	n.id = ue.NETWORK_OPERATOR_ID;
END;

$$ LANGUAGE plpgsql;
----------------------------------------------------------


CREATE OR REPLACE
FUNCTION user_directory.get_users_2(
    OUT p_resp_code varchar ,
    OUT ref_cursor REFCURSOR
) 
AS $$
DECLARE
--p_resp_code text;
--ref_cursor REFCURSOR := 'mycursor';

BEGIN
	p_resp_code := 'OK';

OPEN ref_cursor FOR
SELECT
	ue.id,
	ue.code,
	c.city_name,
	ue.address,
	ue."location",
	n.network_operator_name,
	ue.work_schedule,
	p.phone_number,
	ue.chief
FROM
	user_directory.user_entity ue
LEFT JOIN user_directory.city c ON
	c.id = ue.city_id
LEFT JOIN user_directory.phones p ON
	p.user_entity_id = ue.id
LEFT JOIN user_directory.network_operator n ON
	n.id = p.user_entity_id;
END;

$$ LANGUAGE plpgsql;
-----------------------------------------------------------

CREATE OR REPLACE
FUNCTION user_directory.save_user(
	IN p_id BIGINT, 
    IN p_code VARCHAR(50),
    IN p_city_id BIGINT,
    IN p_address VARCHAR(200),
    IN p_location VARCHAR(200),
    IN p_network_operator_id BIGINT,
    IN p_work_schedule VARCHAR(200),
    IN p_phone VARCHAR(100),
    IN p_chief VARCHAR(200),
    OUT p_resp_code varchar ,
    OUT p_resp_message varchar
--OUT ref_cursor REFCURSOR
) 
AS $$
DECLARE
	p_phone_id BIGINT;
--ref_cursor REFCURSOR := 'mycursor';
BEGIN
	
	
IF p_id IS NULL THEN
--вставка нового
    INSERT
	INTO
	user_directory.user_entity (
    code,
	city_id,
	address,
	"location",
	network_operator_id,
	work_schedule,
	chief)
VALUES (
    p_code,
    p_city_id,
    p_address,
    p_location,
    p_network_operator_id,
    p_work_schedule,
    p_chief)
    RETURNING id
INTO
	p_id;

IF p_phone IS NOT NULL THEN
    INSERT
	INTO
	user_directory.PHONES (PHONE_NUMBER,
	USER_ENTITY_ID)
VALUES (p_phone,
p_id) 
    RETURNING id
INTO
	p_phone_id;

UPDATE
	user_directory.USER_ENTITY
SET
	PHONE_ID = p_phone_id
WHERE
	id = p_id;
END IF;
END IF;
--    p_out_id := p_id;
p_resp_code := 'OK';

p_resp_message := 'Строка сохранена успешно. id = ' || p_id;
END;

$$ LANGUAGE plpgsql;
-------------------------------------------------------------
-- Проверить работу процедур вручную из DBeaver
--SELECT user_directory.get_users_2();
--SELECT user_directory.SAVE_USER(:p_id, :p_code, :p_city_id, :p_address, :p_location, :p_network_operator_id, :p_work_schedule, :p_phone_id, :p_chief);
------------------------------------------------------------------
CREATE OR REPLACE
FUNCTION user_directory.save_phone(
	IN p_phone VARCHAR(20),
    IN p_user_entity_id BIGINT , 
    OUT p_resp_code VARCHAR ,
    OUT p_resp_message VARCHAR
--OUT ref_cursor REFCURSOR
) 
AS $$
DECLARE
	p_id BIGINT;
--ref_cursor REFCURSOR := 'mycursor';
BEGIN
--вставка нового
    INSERT
	INTO
	user_directory.PHONES (
    phone_number,
	USER_ENTITY_ID
    )
VALUES (
    p_phone,
    p_user_entity_id
    )
    RETURNING id
INTO
	p_id;

p_resp_code := 'OK';

p_resp_message := 'Строка сохранена успешно. id = ' || p_id;

EXCEPTION
WHEN OTHERS THEN
  p_resp_code := 'error';

p_resp_message := 'Не удалось сохранить телефонный номер ' || p_phone;
END;

$$ LANGUAGE plpgsql;
-----------------------------------------------------------------

CREATE OR REPLACE
FUNCTION user_directory.get_user_phones(
	IN p_user_entity_id BIGINT ,
    OUT p_resp_code varchar ,
    OUT ref_cursor REFCURSOR
) 
AS $$
DECLARE 

BEGIN 
	p_resp_code := 'OK';

IF p_user_entity_id IS NOT NULL THEN
    OPEN ref_cursor FOR
SELECT
	p.PHONE_NUMBER
FROM
	user_directory.PHONES P
WHERE
	p.USER_ENTITY_ID = p_user_entity_id;
END IF;
END;

$$ LANGUAGE plpgsql;
-----------------------------------------------------------------
CREATE OR REPLACE FUNCTION user_directory.get_user_by(p_user_entity_id BIGINT) 
RETURNS text AS 
$$
DECLARE 
 p_user_info JSONB;
BEGIN 
    
SELECT
	json_build_object (
    'id', ue.id,
	'code', ue.code,
	'cityName', c.city_name,
	'address', ue.address,
	'location', ue."location",
	'networkOperatorName', n.network_operator_name,
	'workSchedule', ue.work_schedule,
	'phoneNumber', ue.PHONE_ID ,
	'chief', ue.chief) 
	INTO p_user_info
FROM
	user_directory.user_entity ue
LEFT JOIN user_directory.city c ON
	c.id = ue.city_id
LEFT JOIN user_directory.phones p ON
	p.user_entity_id = ue.id
LEFT JOIN user_directory.network_operator n ON
	n.id = ue.NETWORK_OPERATOR_ID
WHERE ue.ID = p_user_entity_id;

RETURN p_user_info;

END;
$$ 
LANGUAGE plpgsql;


