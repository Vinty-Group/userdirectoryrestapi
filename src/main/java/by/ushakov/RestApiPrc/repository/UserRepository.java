package by.ushakov.RestApiPrc.repository;

import by.ushakov.RestApiPrc.models.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

//    @Query(value = "SELECT user_directory.get_user_by(p_user_entity_id => :userId)", nativeQuery = true)
//    UserEntityDTO findUserById(Long userId);

    // Сразу получаем JSON из БД
    @Query(value = "SELECT hibernate_demo_db.user_directory.get_user_by2(?1)", nativeQuery = true)
    String findUserById(Long userId);

    void deleteById(Long id);
}
