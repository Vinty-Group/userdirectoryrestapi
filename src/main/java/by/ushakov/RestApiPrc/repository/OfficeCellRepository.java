package by.ushakov.RestApiPrc.repository;

import by.ushakov.RestApiPrc.models.OfficeCellEntity;
import by.ushakov.RestApiPrc.models.sheets.OfficeCell;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeCellRepository extends JpaRepository<OfficeCellEntity, Long> {

    void deleteById(Long id);

}
