package by.ushakov.RestApiPrc.repository.energy;

import by.ushakov.RestApiPrc.models.energy.Telemetr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TelemetrRepository extends JpaRepository<Telemetr, Long> {

    List<Telemetr> findAll();

    Optional<Telemetr> findById(Long id);
}
