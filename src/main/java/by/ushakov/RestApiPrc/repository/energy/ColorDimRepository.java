package by.ushakov.RestApiPrc.repository.energy;

import by.ushakov.RestApiPrc.models.energy.ColorDim;
import by.ushakov.RestApiPrc.models.energy.Telemetr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColorDimRepository extends JpaRepository<ColorDim, Long> {

    List<ColorDim> findAll();

}
