package by.ushakov.RestApiPrc.exell;

import by.ushakov.RestApiPrc.models.sheets.OfficeCell;
import by.ushakov.RestApiPrc.models.sheets.OfficeCellResponse;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

@Component
@RequiredArgsConstructor
public class MemorySheet {

    private final Sheets sheets;

    private Map<Integer, List<String>> exellBook1 = new HashMap<>();
    private Map<Integer, List<String>> exellBook2 = new HashMap<>();
    private Map<Integer, List<String>> exellBook3 = new HashMap<>();
    private Map<String, OfficeCellResponse> officeBook = new HashMap<>();

    public void fillMapFromExell() {
        try {
            exellBook1 = sheets.readingFile(0, false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            exellBook2 = sheets.readingFile(1, false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            exellBook3 = sheets.readingFile(2, false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //  вместе с моделью
        try {
            officeBook = sheets.readingFileModel(0, false);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<Integer, List<String>> getExellBook(int index) {
        switch (index) {
            case 1:
                return exellBook1;
            case 2:
                return exellBook2;
            case 3:
                return exellBook3;
            default:
                HashMap<Integer, List<String>> result = new HashMap<>();
                String message = "Книги в Excell-документе с таким порядковым номером не существует";
                ArrayList<String> list = new ArrayList<>(Collections.singleton(message));
                result.put(0, list);
                return result;
        }
    }

    public Map<String, OfficeCellResponse> getOfficeBook() {
        return officeBook;
    }

    public Map<String, OfficeCellResponse> getOfficeBook(String find) {
        // если не указано ничего с поиске - то вернуть все
        if (find == null || find.isEmpty()) {
            return officeBook;
        }
        find = find.toLowerCase();
        Map<String, OfficeCellResponse> responseMap = new HashMap<>();
        for (Map.Entry<String, OfficeCellResponse> entry : officeBook.entrySet()) {
            String key = entry.getKey();
            OfficeCellResponse entryValue = entry.getValue();

            OfficeCell officeCells = entryValue.getOfficeCells();
            String jobTitle = officeCells.getJobTitle();
            String fullName = officeCells.getFullName();
            String phone = officeCells.getPhone();
            if (fullName.toLowerCase().contains(find)
                    || entryValue.getTypeRecord().equals("$$header")
                    || entryValue.getTypeRecord().equals("$$subHeader"))  {
                responseMap.put(key, entry.getValue());
            }

            if (jobTitle.toLowerCase().contains(find)) {
                responseMap.put(key, entry.getValue());
            }

            // удалить все пробелы и дефисы и все символы, кроме цифр
            String findMy = find.replaceAll("[\\s-]+", "");
            String phoneMy = phone.replaceAll("[\\s-]+", "");
            phoneMy = phoneMy.replaceAll("[^\\d]", "");
            if (phoneMy.contains(findMy)) {
                responseMap.put(key, entry.getValue());
            }
        }
        return responseMap;
    }
}
