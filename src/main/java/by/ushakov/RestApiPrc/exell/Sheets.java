package by.ushakov.RestApiPrc.exell;

import by.ushakov.RestApiPrc.models.sheets.OfficeCell;
import by.ushakov.RestApiPrc.models.sheets.OfficeCellResponse;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

@Component
public class Sheets {
    String fileLocation = "books.xlsx";

    public Map<Integer, List<String>> readingFile(int index, boolean isReadFirstRecord) throws IOException {
        // открываем файл
        FileInputStream file = new FileInputStream(new File(fileLocation));
        Workbook workbook = new XSSFWorkbook(file);

        // количество листов всего в Эксель-документе
        int numberOfSheets = workbook.getNumberOfSheets();
        if (index >= numberOfSheets) {
            return new HashMap<>();
        }
        return getDataFromSheet(index, workbook, isReadFirstRecord);
    }

    private static Map<Integer, List<String>> getDataFromSheet(int index, Workbook workbook, boolean isReadFirstRecord) {
        // получаем первую таблицу в документе
        Sheet sheet = workbook.getSheetAt(index);
        DataFormatter dataFormatter = new DataFormatter();

        Map<Integer, List<String>> data = new HashMap<>();
        int i = 0;

        for (Row row : sheet) {

            data.put(i, new ArrayList<String>());
            for (int j = 0; j < row.getLastCellNum(); j++) {
                Cell cell = row.getCell(j, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                data.get(i).add(cell.toString());
            }
            i++;
        }
        if (!isReadFirstRecord) {
            data.remove(0);
        }
        return data;
    }

    public Map<String, OfficeCellResponse> readingFileModel(int index, boolean isReadFirstRecord) throws IOException {

        FileInputStream file = new FileInputStream(fileLocation);
        Workbook workbook = new XSSFWorkbook(file);

        int numberOfSheets = workbook.getNumberOfSheets();
        if (index >= numberOfSheets) {
            return new HashMap<>();
        }
        return getDataFromSheetModel(index, workbook, isReadFirstRecord);
    }

    private Map<String, OfficeCellResponse> getDataFromSheetModel(int index, Workbook workbook, boolean isReadFirstRecord) {
        // получаем первую таблицу в документе
        Sheet sheet = workbook.getSheetAt(index);

        Map<String, List<String>> data = new HashMap<>();
        int i = 0;

        for (Row row : sheet) {
            data.put(String.valueOf(i), new ArrayList<>());

            for (int j = 0; j < row.getLastCellNum(); j++) {
                // читаем ячейку, если пусто присваиваем пустую строку
                Cell cell = row.getCell(j, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                data.get(String.valueOf(i)).add(cell.toString());

            }
            i++;
        }
        if (!isReadFirstRecord) {
            data.remove(String.valueOf(0));
        }

        return mapFromDataToResult(data);
    }

    private Map<String, OfficeCellResponse> mapFromDataToResult(Map<String, List<String>> data) {
        Map<String, OfficeCellResponse> result = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : data.entrySet()) {
            String key = entry.getKey();
            List<String> values = entry.getValue();
            // создаем новый объект OfficeCell
            OfficeCell officeCell = OfficeCell.builder()
                    .jobTitle(values.get(0))
                    .responsibleFor(values.get(1))
                    .fullName(values.get(2))
                    .phone(values.get(3))
                    .build();
            // добавляем объект в OfficeCellResponse как составной
            OfficeCellResponse response = OfficeCellResponse.builder()
                    .officeCells(officeCell)
                    .typeRecord(isHeader(values.get(0)))
                    .build();
            // добавляем список в результирующую мапу
            result.put(key, response);
        }
        return result;
    }

    public String isHeader(String s) {
        String regexHeader = "^\\d*[.] .+";
        String regexSubHeader = "^\\d*[.]\\d*. .+";
        if (s.matches(regexSubHeader)) {
            return "$$subHeader";
        }
        if (s.matches(regexHeader)) {
            return "$$header";
        }
        return "$$basic";
    }


//    public void createExellFile() {
//        Workbook workbook = new XSSFWorkbook();
//        Sheet sheet = workbook.createSheet("Sheet1");
//        Row row = sheet.createRow(0);
//        Cell cell = row.createCell(0);
//        cell.setCellValue("Привет Ярик!!!!!!");
//        System.out.println(" Таблица EXELL создана.");
//
//        String projectRootPath = System.getProperty("user.dir");
//
//// Относительный путь к файлу
//        String filePath = projectRootPath + "/example.xlsx";
//
//// Сохранение файла
//        try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
//            workbook.write(outputStream);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

}
