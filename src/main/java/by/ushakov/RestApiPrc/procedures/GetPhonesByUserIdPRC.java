package by.ushakov.RestApiPrc.procedures;

import by.ushakov.RestApiPrc.models.PhoneDTO;
import by.ushakov.RestApiPrc.models.UserEntitySaveDTO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@Component
// описываем процедуру в БД.Где находится. Какие параметры для выполнения процедуры из слоя DAO
public class GetPhonesByUserIdPRC {

    private final String usersEntitySchemaName = "user_directory"; // схема где лежит процедура в БД
    private final String usersEntityPrcName = "get_user_phones"; // название процедуры в БД

    // названия параметров на вход процедуры IN
    private final String  InParamName_1 = "p_user_entity_id";

    // названия параметров на выход процедуры OUT
    private final String OutParamName_2 = "ref_cursor";
    private final String OutParamName_1 = "p_resp_code";

    // Поля RESULTSET, которые возвращает REFCURSOR. Порядок ВАЖЕН! Именно в таком виде они отображаются в SELECT.
    private final String resultSetField_1 = "phone_number";

    public void fillOUTParameters(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getUsersEntitySchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getUsersEntityPrcName()); // Имя хранимой процедуры
        jdbcCall.declareParameters(
                new SqlOutParameter(OutParamName_1, Types.VARCHAR),
                new SqlOutParameter(OutParamName_2, Types.REF_CURSOR)); // Имя выходного параметра refcursor
    }

    public Map<String, Object> fillINParameters(SimpleJdbcCall jdbcCall, Long id) {
        Map<String, Object> parameters = new HashMap<>(); // Для входных параметров если есть такие в процедуре IN
        parameters.put(InParamName_1, id);
        return parameters;
    }

    // Из RESULTSET получаем массив Строк из телефонов пользователя
    public List<PhoneDTO> getPhoneNumbers(List<Map<String, Object>> resultSet) {
        List<PhoneDTO> list = new ArrayList<>();
        for (Map<String, Object> row : resultSet) {
            list.add(PhoneDTO.builder()
                    .phoneNumber(String.valueOf(row.get("phone_number")))
                    .build());
        }
        return list;
    }
}
