package by.ushakov.RestApiPrc.procedures;

import by.ushakov.RestApiPrc.models.OfficeCellDTO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@Component
// описываем процедуру в БД.Где находится. Какие параметры для выполнения процедуры из слоя DAO
public class GetOfficeCellPRC {
    //    private final String getUsersEntityPrcName = "user_directory.get_office_cell";
    private final String officeCellSchemaName = "user_directory"; // схема где лежит процедура в БД
    private final String officeCellPrcName = "get_office_cell"; // название процедуры в БД

    // названия параметров на выход процедуры
    private final String OutParamName_1 = "ref_cursor";

    // Поля RESULTSET, которые возвращает REFCURSOR. Порядок ВАЖЕН! Именно в таком виде они отображаются в SELECT.
    private final String resultSetField_1 = "id";
    private final String resultSetField_2 = "jobtitle";
    private final String resultSetField_3 = "responsiblefor";
    private final String resultSetField_4 = "fullname";
    private final String resultSetField_5 = "phone";

    // заполняем параметрами SimpleJdbcCall. Эти параметры будут использованы при вызове процедуры.
    public void fillOUTParameters(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getOfficeCellSchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getOfficeCellPrcName()); // Имя хранимой процедуры
        jdbcCall.declareParameters(
                new SqlOutParameter(OutParamName_1, Types.REF_CURSOR)); // Имя выходного параметра refcursor
    }

    // Маппинг. Что бы все поля которые пришли в REFCURSOR из БД - легли в объект Java.
    public List<OfficeCellDTO> getOfficeCell(List<Map<String, Object>> resultSet) {
        List<OfficeCellDTO> list = new ArrayList<>();
        for (Map<String, Object> row : resultSet) {
            list.add(OfficeCellDTO.builder()
                    .id((Long) row.get(resultSetField_1))
                    .jobTitle((String) row.get(resultSetField_2))
                    .responsibleFor((String) row.get(resultSetField_3))
                    .fullName((String) row.get(resultSetField_4))
                    .phone((String) row.get(resultSetField_5))
                    .build());
        }
        return list;
    }
}
