package by.ushakov.RestApiPrc.procedures;

import by.ushakov.RestApiPrc.models.UserEntitySaveDTO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Data
@RequiredArgsConstructor
@Component
// описываем процедуру в БД.Где находится. Какие параметры для выполнения процедуры из слоя DAO
public class SaveUserPRC {

    private final String usersEntitySchemaName = "user_directory"; // схема где лежит процедура в БД
    private final String usersEntityPrcName = "save_user"; // название процедуры в БД

    // названия параметров на вход процедуры
    private final String INParamName_1 = "p_id";
    private final String INParamName_2 = "p_code";
    private final String INParamName_3 = "p_city_id";
    private final String INParamName_4 = "p_address";
    private final String INParamName_5 = "p_location";
    private final String INParamName_6 = "p_network_operator_id";
    private final String INParamName_7 = "p_work_schedule";
    private final String INParamName_8 = "p_phone";
    private final String INParamName_9 = "p_chief";

    // названия параметров на выход процедуры
    private final String OutParamName_1 = "p_resp_code";
    private final String OutParamName_2 = "p_resp_message";

    // Поля RESULTSET, которые возвращает REFCURSOR. Порядок ВАЖЕН! Именно в таком виде они отображаются в SELECT.
    // Тут их пока нет. Мы сохраняем пользователя. Поэтому что либо принимать из RESULTSET - не обязательно.

    // заполняем параметрами SimpleJdbcCall. Эти параметры будут использованы при вызове процедуры.
    public void fillOUTParameters(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getUsersEntitySchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getUsersEntityPrcName()); // Имя хранимой процедуры
        jdbcCall.declareParameters(
                new SqlOutParameter(OutParamName_1, Types.VARCHAR),
                new SqlOutParameter(OutParamName_2, Types.VARCHAR));
    }

    public Map<String, Object> fillINParameters(SimpleJdbcCall jdbcCall, UserEntitySaveDTO mappedClass) {
        Map<String, Object> parameters = new HashMap<>(); // Для входных параметров если есть такие в процедуре IN
                parameters.put(INParamName_1, mappedClass.getId());
                parameters.put(INParamName_2, mappedClass.getCode());
                parameters.put(INParamName_3, mappedClass.getCityId());
                parameters.put(INParamName_4, mappedClass.getAddress());
                parameters.put(INParamName_5, mappedClass.getLocation());
                parameters.put(INParamName_6, mappedClass.getNetworkOperatorId());
                parameters.put(INParamName_7, mappedClass.getWorkSchedule());
                parameters.put(INParamName_8, mappedClass.getPhoneNumber());
                parameters.put(INParamName_9, mappedClass.getChief());
                return parameters;
    }
}
