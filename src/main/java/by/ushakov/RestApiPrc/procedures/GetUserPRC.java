package by.ushakov.RestApiPrc.procedures;

import by.ushakov.RestApiPrc.models.UserEntityDTO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Data
@RequiredArgsConstructor
@Component
// описываем процедуру в БД.Где находится. Какие параметры для выполнения процедуры из слоя DAO
public class GetUserPRC {

    //    private final String getUsersEntityPrcName = "user_directory.get_users_h";
    private final String usersEntitySchemaName = "user_directory"; // схема где лежит процедура в БД
    private final String usersEntityPrcName = "get_users"; // название процедуры в БД

    // названия параметров на выход процедуры
    private final String OutParamName_1 = "p_resp_code";
    private final String OutParamName_2 = "ref_cursor";

    // Поля RESULTSET, которые возвращает REFCURSOR. Порядок ВАЖЕН! Именно в таком виде они отображаются в SELECT.
    private final String resultSetField_1 = "id";
    private final String resultSetField_2 = "code";
    private final String resultSetField_3 = "city_name";
    private final String resultSetField_4 = "address";
    private final String resultSetField_5 = "location";
    private final String resultSetField_6 = "network_operator_name";
    private final String resultSetField_7 = "work_schedule";
    private final String resultSetField_8 = "phone_id";
    private final String resultSetField_9 = "chief";

    // заполняем параметрами SimpleJdbcCall. Эти параметры будут использованы при вызове процедуры.
    public void fillOUTParameters(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getUsersEntitySchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getUsersEntityPrcName()); // Имя хранимой процедуры
        jdbcCall.declareParameters(
                new SqlOutParameter(OutParamName_1, Types.VARCHAR),
                new SqlOutParameter(OutParamName_2, Types.REF_CURSOR)); // Имя выходного параметра refcursor
    }

    // Маппинг. Что бы все поля которые пришли в REFCURSOR из БД - легли в объект Java.
    public List<UserEntityDTO> getUserEntities(List<Map<String, Object>> resultSet) {
        List<UserEntityDTO> list = new ArrayList<>();
        for (Map<String, Object> row : resultSet) {
            list.add(UserEntityDTO.builder()
                    .id((Long) row.get(resultSetField_1))
                    .code((String) row.get(resultSetField_2))
                    .cityName((String) row.get(resultSetField_3))
                    .address((String) row.get(resultSetField_4))
                    .location((String) row.get(resultSetField_5))
                    .networkOperatorName((String) row.get(resultSetField_6))
                    .workSchedule((String) row.get(resultSetField_7))
                    .phoneNumber((Long) row.get(resultSetField_8))
                    .chief((String) row.get(resultSetField_9))
                    .build());
        }
        return list;
    }



    // если пользуемся Хибернейтом для автомаппинга
    public void setDescriptionParam(StoredProcedureQuery query) {
        query.registerStoredProcedureParameter(1, ResultSet.class, ParameterMode.REF_CURSOR);
    }

    // если пользуемся Хибернейтом для автомаппинга
    public void setIN(StoredProcedureQuery query, UserEntityDTO mappedClass) {
    }
}
