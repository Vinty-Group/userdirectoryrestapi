package by.ushakov.RestApiPrc.dao;

import by.ushakov.RestApiPrc.models.OfficeCellDTO;
import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.procedures.GetOfficeCellPRC;
import by.ushakov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@Repository
@RequiredArgsConstructor
// Слой DAO. Лезет в БД. Вызывает процедуры. Дает на вход и выход параметры необходимые для ее работы
public class OfficeCellDao {
    private final JdbcTemplate jdbcTemplate;
    private final GetOfficeCellPRC getOfficeCellPRC;

    public ResponseList<OfficeCellDTO> getOfficeCell() {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        getOfficeCellPRC.fillOUTParameters(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> parameters = new HashMap<>(); // Для входных параметров если есть такие в процедуре IN

        // выполняем процедуру с параметрами
        Map<String, Object> result = jdbcCall.execute(parameters); //parameters = входные параметры если они есть у процедуры

        // REFCURSOR OUT - это сами данные из SELECT
        List<Map<String, Object>> resultSet = (List<Map<String, Object>>) result.get("ref_cursor");

        List<OfficeCellDTO> list = getOfficeCellPRC.getOfficeCell(resultSet);
        ResponseList<OfficeCellDTO> responseList = new ResponseList<>();
        responseList.setContent(list);
        responseList.setRespCode("200");
        responseList.setRespMess("Данные успешно получены");
        return responseList;
    }
}
