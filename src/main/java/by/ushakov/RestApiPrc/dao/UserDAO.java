package by.ushakov.RestApiPrc.dao;

import by.ushakov.RestApiPrc.models.PhoneDTO;
import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntityReleseDTO;
import by.ushakov.RestApiPrc.models.UserEntitySaveDTO;
import by.ushakov.RestApiPrc.procedures.GetPhonesByUserIdPRC;
import by.ushakov.RestApiPrc.procedures.GetUserPRC;
import by.ushakov.RestApiPrc.procedures.SaveUserPRC;
import by.ushakov.RestApiPrc.repository.UserRepository;
import by.ushakov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.*;

@Log4j2
@Repository
@RequiredArgsConstructor
// Слой DAO. Лезет в БД. Вызывает процедуры. Дает на вход и выход параметры необходимые для ее работы
public class UserDAO {

    private final JdbcTemplate jdbcTemplate;
    private final GetUserPRC getUserPrc;
    private final SaveUserPRC saveUserPRC;
    private final GetPhonesByUserIdPRC getPhonesByUserIdPRC;
    private final UserRepository userRepository;

    //    @PersistenceContext
    //    private EntityManager em;
    public ResponseList<UserEntityDTO> getUsers(UserEntityDTO mappedClass) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        getUserPrc.fillOUTParameters(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> parameters = new HashMap<>(); // Для входных параметров если есть такие в процедуре IN
//        parameters.put("user_entity_id", inputParam);

        // выполняем процедуру с параметрами
        Map<String, Object> result = jdbcCall.execute(parameters); //parameters = входные параметры если они есть у процедуры

        // REFCURSOR OUT - это сами данные из SELECT
        List<Map<String, Object>> resultSet = (List<Map<String, Object>>) result.get("ref_cursor");

        List<UserEntityDTO> list = getUserPrc.getUserEntities(resultSet);
        ResponseList<UserEntityDTO> responseList = new ResponseList<>();
        responseList.setContent(list);
        responseList.setRespCode((String) result.get("p_resp_code")); // String OUT параметр из процедуры get_users
        return responseList;
    }


    public ResponseList<UserEntitySaveDTO> saveUser(UserEntitySaveDTO mappedClass) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        saveUserPRC.fillOUTParameters(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> parameters = saveUserPRC.fillINParameters(jdbcCall, mappedClass);

        // выполняем процедуру с параметрами
        Map<String, Object> result = jdbcCall.execute(parameters); //parameters = входные параметры если они есть у процедуры

        ResponseList<UserEntitySaveDTO> responseList = new ResponseList<>();
        responseList.setRespCode((String) result.get("p_resp_code")); // String OUT параметр из процедуры save_users
        responseList.setRespMess((String) result.get("p_resp_message")); // String OUT параметр из процедуры save_users
        return responseList;
    }


//    public ResponseList<UserEntity> getUserBy(UserEntity mappedClass) throws SQLException {
//        StoredProcedureQuery query = em.createStoredProcedureQuery(userPrc.getGetUsersEntityPrcName(), UserEntity.class);
//        userPrc.setDescriptionParam(query);
//        query.execute();
//        return ResponseList.builder()
//                .content(query.getResultList())
//                .build();
//    }

    public ResponseList<UserEntityDTO> deleteUser(Long id) {
        userRepository.deleteById(id);
        return dataFromFakeBD();
    }

    public ResponseList<UserEntityDTO> dataFromFakeBD() {
        ResponseList<UserEntityDTO> responseList = new ResponseList<>();
        List<UserEntityDTO> listUserEntityDTO = new ArrayList<>();
        listUserEntityDTO.add(UserEntityDTO.builder().code("Интернет магазин").build());
        responseList.setContent(listUserEntityDTO);
        responseList.setRespCode("OK");
        responseList.setRespMess("OK");
        return responseList;
    }

    public List<PhoneDTO> getPhonesByUserId(Long phoneNumber) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        getPhonesByUserIdPRC.fillOUTParameters(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> fillINParameters = getPhonesByUserIdPRC.fillINParameters(jdbcCall, phoneNumber);
        // выполняем процедуру с параметрами
        Map<String, Object> result = jdbcCall.execute(fillINParameters); //parameters = входные параметры если они есть у процедуры
        // REFCURSOR OUT - это сами данные из SELECT
        List<Map<String, Object>> resultSet = (List<Map<String, Object>>) result.get("ref_cursor");
        return getPhonesByUserIdPRC.getPhoneNumbers(resultSet);
    }
}
