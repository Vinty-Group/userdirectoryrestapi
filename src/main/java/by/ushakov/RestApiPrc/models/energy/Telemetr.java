package by.ushakov.RestApiPrc.models.energy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table (name = "telemetr")
public class Telemetr {

    @Id
    private Long id;
    @Column(name = "codetelemetr")
    private String codeTelemetr;

    @Column(name = "valuetelemetr")
    private String valueTelemetr;

}
