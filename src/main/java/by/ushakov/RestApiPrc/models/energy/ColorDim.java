package by.ushakov.RestApiPrc.models.energy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "color_dim")

public class ColorDim {
    @Id
    private Long id;

    @Column(name = "color_name")
    private String event;

    @Column(name = "color_value")
    private String colorValue;
}




