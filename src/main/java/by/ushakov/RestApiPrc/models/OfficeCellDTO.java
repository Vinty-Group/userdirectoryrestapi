package by.ushakov.RestApiPrc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
// Для автомаппинга контроллера, что бы получить JSON
public class OfficeCellDTO {
    private Long id;
    private String jobTitle;
    private String responsibleFor;
    private String fullName;
    private String phone;
}
