package by.ushakov.RestApiPrc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
// Для автомаппинга контроллера, что бы получить JSON
public class UserEntityDTO {
    @Id
    private Long id;
    private String code;
    private String cityName;
    private String address;
    private String location;
    private String networkOperatorName;
    private String workSchedule;
    private Long phoneNumber;
    private String chief;
}
