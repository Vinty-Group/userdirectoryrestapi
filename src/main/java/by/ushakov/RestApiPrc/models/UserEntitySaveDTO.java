package by.ushakov.RestApiPrc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
// Для автомаппинга контроллера, что бы сохранить в БД
public class UserEntitySaveDTO {
    @Id
    private Long id;
    private String code;
    private Long cityId;
    private String address;
    private String location;
    private Long networkOperatorId;
    private String workSchedule;
    private String phoneNumber;
    private String chief;
}
