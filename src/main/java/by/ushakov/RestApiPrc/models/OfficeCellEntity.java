package by.ushakov.RestApiPrc.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "officecell")
public class OfficeCellEntity {
    @Id
    private Long id;
    @Column(name = "jobtitle")
    private String jobTitle;
    @Column(name = "responsiblefor")
    private String responsibleFor;
    @Column(name = "fullname")
    private String fullName;
    @Column(name = "phone")
    private String phone;
}
