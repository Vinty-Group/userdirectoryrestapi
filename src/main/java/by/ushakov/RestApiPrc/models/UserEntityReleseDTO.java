package by.ushakov.RestApiPrc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@Entity
@Builder
@Component
public class UserEntityReleseDTO {
//    @Id
    private Long id;
    private String code;
    private String cityName;
    private String address;
    private String location;
    private String networkOperatorName;
    private String workSchedule;
    private List<PhoneDTO> phoneNumber;
    private String chief;
}
