package by.ushakov.RestApiPrc.models.sheets;

import lombok.Data;

@Data
public class Cells {
    private String code;
    private String city;
    private String address;
    private String place;
    private String operator;
    private String workTime;
    private String phone;
    private String bossSector;
}
