package by.ushakov.RestApiPrc.models.sheets;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OfficeCell {
    private String jobTitle;
    private String responsibleFor;
    private String fullName;
    private String phone;
}
