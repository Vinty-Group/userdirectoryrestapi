package by.ushakov.RestApiPrc.models.sheets;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class OfficeCellResponse {
    private String typeRecord;
    private OfficeCell officeCells;
}
