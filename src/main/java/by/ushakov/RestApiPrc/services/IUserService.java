package by.ushakov.RestApiPrc.services;

import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntityReleseDTO;
import by.ushakov.RestApiPrc.utils.ResponseList;

import java.util.Optional;

public interface IUserService {

    ResponseList<UserEntityReleseDTO> getUsersById(Long userId);

}
