package by.ushakov.RestApiPrc.services;

import by.ushakov.RestApiPrc.dao.UserDAO;
import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntityReleseDTO;
import by.ushakov.RestApiPrc.models.UserEntitySaveDTO;
import by.ushakov.RestApiPrc.repository.UserRepository;
import by.ushakov.RestApiPrc.utils.JsonHelper;
import by.ushakov.RestApiPrc.utils.ResponseList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements IUserService{

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final UserDAO userDao;
    private final JsonHelper jsonHelper;
    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    public ResponseList<UserEntityReleseDTO> getUsers(UserEntityDTO userEntityDTO) {
        // Получаем всех пользователей с id-телефона по которому будем вытаскивать все телефоны
        ResponseList<UserEntityDTO> responseList = userDao.getUsers(userEntityDTO);
        // Заполним объект который в итоге превратится в JSON в контроллере
        ResponseList<UserEntityReleseDTO> result = new ResponseList<>();

        List<UserEntityReleseDTO> list = new ArrayList<>();
        // Для каждого пользователя создаем точно такого же, но с заполненными номерами телефонов
        responseList.getContent().forEach(userFromDAO ->
                list.add(UserEntityReleseDTO.builder()
                        .id(userFromDAO.getId())
                        .code(userFromDAO.getCode())
                        .cityName(userFromDAO.getCityName())
                        .address(userFromDAO.getAddress())
                        .location(userFromDAO.getLocation())
                        .networkOperatorName(userFromDAO.getNetworkOperatorName())
                        .workSchedule(userFromDAO.getWorkSchedule())
                        .phoneNumber(userDao.getPhonesByUserId(userFromDAO.getId()))
                        .chief(userFromDAO.getChief())
                        .build()));
        result.setContent(list);
        result.setRespCode(responseList.getRespCode());
        result.setRespMess(responseList.getRespMess());
        return result;
    }

    @Transactional
    public ResponseList<UserEntitySaveDTO> saveUser(String json) {
        if (Objects.nonNull(json) && jsonHelper.isValid(json)) {
            ObjectMapper objectMapper = new ObjectMapper();

            UserEntitySaveDTO user = new UserEntitySaveDTO();
            try {
                user = objectMapper.readValue(json, UserEntitySaveDTO.class);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            return userDao.saveUser(UserEntitySaveDTO.builder()
                    .code(user.getCode())
                    .cityId(user.getCityId())
                    .address(user.getAddress())
                    .location(user.getLocation())
                    .networkOperatorId(user.getNetworkOperatorId())
                    .workSchedule(user.getWorkSchedule())
                    .phoneNumber(user.getPhoneNumber())
                    .chief(user.getChief())
                    .build());
        }
        log.error("JSON: " + json, () -> new RuntimeException("JSON is NOT valid!"));
        return ResponseList.<UserEntitySaveDTO>builder()
                .respCode("ERROR")
                .respMess("JSON is NOT valid!")
                .build();
    }

    @Transactional
    public ResponseList<UserEntityDTO> deleteUser(Long id) {
        return userDao.deleteUser(id);
    }

    public ResponseList<UserEntityReleseDTO> getUsersById(Long userId) {
        ResponseList<UserEntityReleseDTO> responseList = new ResponseList<>();
        List<UserEntityReleseDTO> list = new ArrayList<UserEntityReleseDTO>();
        String json = userRepository.findUserById(userId);
        try {
            UserEntityReleseDTO userEntityReleseDTO = objectMapper.readValue(json, UserEntityReleseDTO.class);
            list.add(userEntityReleseDTO);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        responseList.setContent(list);
        return responseList;
    }


//    public ResponseList<UserEntitySaveDTO> saveUser(String json) {
//        return null;
//    }
}






