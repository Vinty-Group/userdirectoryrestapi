package by.ushakov.RestApiPrc.services;

import by.ushakov.RestApiPrc.models.OfficeCellDTO;
import by.ushakov.RestApiPrc.models.UserEntityReleseDTO;
import by.ushakov.RestApiPrc.utils.ResponseList;

public interface IOfficeCellService {

    ResponseList<OfficeCellDTO> getOfficeAll();

    void deleteOffice(String officeId);

}
