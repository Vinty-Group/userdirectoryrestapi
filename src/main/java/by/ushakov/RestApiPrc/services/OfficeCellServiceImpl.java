package by.ushakov.RestApiPrc.services;

import by.ushakov.RestApiPrc.dao.OfficeCellDao;
import by.ushakov.RestApiPrc.models.OfficeCellDTO;
import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntityReleseDTO;
import by.ushakov.RestApiPrc.models.sheets.OfficeCell;
import by.ushakov.RestApiPrc.repository.OfficeCellRepository;
import by.ushakov.RestApiPrc.utils.JsonHelper;
import by.ushakov.RestApiPrc.utils.ResponseList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
@RequiredArgsConstructor
public class OfficeCellServiceImpl {
    private final OfficeCellRepository officeCellRepository;

    @Transactional(readOnly = true)
    public ResponseList<OfficeCellDTO> deleteOfficeCell(Long id) {
        officeCellRepository.deleteById(id);
        return dataFromFakeBD();
    }

    public ResponseList<OfficeCellDTO> dataFromFakeBD() {
        ResponseList<OfficeCellDTO> responseList = new ResponseList<>();
        List<OfficeCellDTO> listOfficeCellDTO = new ArrayList<>();

        responseList.setContent(listOfficeCellDTO);
        responseList.setRespCode("OK");
        responseList.setRespMess("OK");
        return responseList;
    }
}
