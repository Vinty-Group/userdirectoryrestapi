package by.ushakov.RestApiPrc.services.energy;

import by.ushakov.RestApiPrc.models.energy.ColorDim;
import by.ushakov.RestApiPrc.models.energy.Telemetr;
import by.ushakov.RestApiPrc.repository.energy.ColorDimRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
@RequiredArgsConstructor
public class ColorDimServiceImpl {

    private final ColorDimRepository colorDimRepository;

    @Transactional
    public List<ColorDim> findAll(){
        return colorDimRepository.findAll();
    }
}
