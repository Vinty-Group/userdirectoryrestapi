package by.ushakov.RestApiPrc.services.energy;

import by.ushakov.RestApiPrc.models.energy.Telemetr;
import by.ushakov.RestApiPrc.repository.energy.TelemetrRepository;
import by.ushakov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@RequiredArgsConstructor
public class TelemetrServiceImpl {

    private final TelemetrRepository telemetrRepository;

    @Transactional
    public List<Telemetr> findAll(){
        return telemetrRepository.findAll();
    }

    @Transactional
    public ResponseList<Telemetr> findbyId(Long id){
        ResponseList<Telemetr> result = new ResponseList<>();
        List<Telemetr> telemetrs = new ArrayList<>();

        Optional<Telemetr> telemetr = telemetrRepository.findById(id);

        if(telemetr.isPresent()){
            telemetrs.add(telemetr.get());
            result.setContent(telemetrs);
            result.setRespCode("Ok!");
            result.setRespMess("Ok!");
        } else {
             result.setRespCode("ERROR");
             result.setRespMess("Такой записи не найдено");
        }
        return result;
    }
}
