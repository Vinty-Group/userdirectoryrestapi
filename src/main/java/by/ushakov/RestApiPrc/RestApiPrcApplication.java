package by.ushakov.RestApiPrc;

import by.ushakov.RestApiPrc.exell.MemorySheet;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;

import java.util.List;
import java.util.Map;

@SpringBootApplication
@Log
@PropertySource(value = "classpath:application.properties")
@ConfigurationPropertiesScan
@EnableJpaRepositories
public class RestApiPrcApplication {

	private static MemorySheet memorySheet = null;

	public RestApiPrcApplication(MemorySheet memorySheet) {
		this.memorySheet = memorySheet;
	}

	public static void main(String[] args) {
		SpringApplication.run(RestApiPrcApplication.class, args);

		log.info("-------------------------------------------------------------------------------");
		log.info("----------------------USER DIRECTORY SERVICE STARTED!--------------------------");
		log.info("-------------------------------------------------------------------------------");

		memorySheet.fillMapFromExell();

		//		Map<Integer, List<String>> exellBook1 = memorySheet.getExellBook(2);
//		exellBook1.forEach((key, value) -> {
//            System.out.print(key + ": ");
//            for (String num : value) {
//                System.out.print(num + " ");
//            }
//            System.out.println();
//        });

	}
}
