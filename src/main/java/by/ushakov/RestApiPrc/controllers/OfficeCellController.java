package by.ushakov.RestApiPrc.controllers;

import by.ushakov.RestApiPrc.models.OfficeCellDTO;
import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntityReleseDTO;
import by.ushakov.RestApiPrc.services.OfficeCellServiceImpl;
import by.ushakov.RestApiPrc.services.UserServiceImpl;
import by.ushakov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;


@RestController
//@RequestMapping("/vinty")
@RequiredArgsConstructor
public class OfficeCellController {

    private final OfficeCellServiceImpl officeCellService;

//    @GetMapping(value = "/getOfficeCell")
//    public ResponseList<OfficeCellDTO> getUsers() throws SQLException {
//        return officeCellService.;
//    }

    @GetMapping(value = "/deleteOfficeCell/{officeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<OfficeCellDTO> deleteOfficeCell(@PathVariable Long officeId) {
        System.out.println("deleteOfficeCell");
        return officeCellService.deleteOfficeCell(officeId);
    }
}
