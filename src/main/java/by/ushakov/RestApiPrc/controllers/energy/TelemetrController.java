package by.ushakov.RestApiPrc.controllers.energy;

import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntityReleseDTO;
import by.ushakov.RestApiPrc.models.energy.ColorDim;
import by.ushakov.RestApiPrc.models.energy.Telemetr;
import by.ushakov.RestApiPrc.services.energy.ColorDimServiceImpl;
import by.ushakov.RestApiPrc.services.energy.TelemetrServiceImpl;
import by.ushakov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@Validated
@RestController
@RequestMapping("/telemetrs")
@RequiredArgsConstructor
public class TelemetrController {

    private final TelemetrServiceImpl service;
    private final ColorDimServiceImpl serviceColor;

//    @GetMapping
//    public List<Telemetr> getAllTelemetr(){
//        return service.findAll();
//    }

    @GetMapping("/color")
    public List<ColorDim> getAllColor(){
        return serviceColor.findAll();
    }

    @GetMapping(value = "/color/{id}")
    public ResponseList<Telemetr> getById(@PathVariable Long id){
        return service.findbyId(id);
    }
}
