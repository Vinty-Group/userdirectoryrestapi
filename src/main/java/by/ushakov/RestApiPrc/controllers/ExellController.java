package by.ushakov.RestApiPrc.controllers;

import by.ushakov.RestApiPrc.exell.MemorySheet;
import by.ushakov.RestApiPrc.models.sheets.OfficeCell;
import by.ushakov.RestApiPrc.models.sheets.OfficeCellResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/exell")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class ExellController {

    //    private final Sheets sheets;
    private final MemorySheet memorySheet;

    @GetMapping(value = "/read")
    public Map<Integer, List<String>> read(@RequestParam int index) {
        return memorySheet.getExellBook(index);
    }

//    @GetMapping(value = "/officeBooks")
//    public Map<String, OfficeCellResponse> readSheetWithModel() {
//        return memorySheet.getOfficeBook();
//    }

    @GetMapping(value = "/officeBook")
    public Map<String, OfficeCellResponse> readSheetWithModelFind(@RequestParam(required = false) String find) {
        return memorySheet.getOfficeBook(find);
    }




//    @GetMapping(value = "/create")
//    public void create(String message) {
//        sheets.createExellFile();
//    }
}
