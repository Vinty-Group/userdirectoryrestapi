package by.ushakov.RestApiPrc.controllers;

import by.ushakov.RestApiPrc.models.UserEntityDTO;
import by.ushakov.RestApiPrc.models.UserEntityReleseDTO;
import by.ushakov.RestApiPrc.models.UserEntitySaveDTO;
import by.ushakov.RestApiPrc.services.UserServiceImpl;
import by.ushakov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@Validated
@RestController
//@RequestMapping("/vinty")
@RequiredArgsConstructor
public class UserController {
    private final UserServiceImpl userServiceImpl;

    @GetMapping(value = "/getUserBy")
    public ResponseList<UserEntityReleseDTO> getUsers(UserEntityDTO vTestPrcGet) throws SQLException {
        System.out.println("getUserBy");
        return userServiceImpl.getUsers(vTestPrcGet);
    }

    @GetMapping(value = "/getUserBy/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<UserEntityReleseDTO> getUsersById(@PathVariable Long userId) {
        System.out.println("getUserBy");
        return userServiceImpl.getUsersById(userId);
    }

    @PostMapping(value = "/saveUser", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<UserEntitySaveDTO> saveUser(@RequestBody String json) {
        return userServiceImpl.saveUser(json);
    }

    // http://localhost:8080/vinty/deleteUser?userId=5
    @GetMapping(value = "/deleteUser/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<UserEntityDTO> deleteUser(@PathVariable Long userId) {
        System.out.println("deleteUser");
        return userServiceImpl.deleteUser(userId);
    }
}




