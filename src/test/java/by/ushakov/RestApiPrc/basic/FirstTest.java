package by.ushakov.RestApiPrc.basic;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
public class FirstTest {
    @Test
    public void myFirstTest(){
        given()
                .when().get("https://pokeapi.co/api/v2/pokemon/charizard")
                .then().statusCode(200);
    }
}
